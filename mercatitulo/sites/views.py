from django.shortcuts import render, render_to_response
from django.template import RequestContext
from proyecto.models import Proyecto
# Create your views here.

def index(request):
	proyectos = Proyecto.objects.all()
	return render(request, 'index.html', {'content': proyectos})

def derechos(request):
	proyectos = ""
	return render(request, 'derechos.html', {'content': proyectos})	
