from django.shortcuts import render
from proyecto.models import Proyecto
from titulo.models import Titulo

def proyectoVista(request, name=0):
	proyectos = Proyecto.objects.get(machineName=name)
	titulos = Titulo.objects.filter(proyecto=proyectos.id)
	proyectoData = {
		"proyectos": proyectos,
		"titulos": titulos,
	}
	return render(request, 'proyecto_detalle.html', {'proyectoData': proyectoData})


def tituloVista(request, name, detail):
	try:
		proyecto = Proyecto.objects.get(machineName=name)
		titulo = Titulo.objects.get(id=detail)
		tituloData = {
			"proyecto": proyecto,
			"titulo": titulo,
		}
	except Titulo.DoesNotExist:
		raise Http404

	return render(request, 'titulo_detalle.html', {'data': tituloData})
