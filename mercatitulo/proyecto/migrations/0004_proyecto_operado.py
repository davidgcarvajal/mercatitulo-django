# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0003_auto_20151202_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='proyecto',
            name='operado',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
