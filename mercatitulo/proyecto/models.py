from __future__ import unicode_literals

from django.db import models

from promotor.models import Promotor
from fiduciaria.models import Fiduciaria

class Proyecto(models.Model):
	nombre = models.CharField(max_length=255)
	machineName = models.CharField(max_length=255, unique=True)
	descripcion = models.TextField(max_length=500)
	promotor = models.ForeignKey(Promotor)
	fiduciaria = models.ForeignKey(Fiduciaria)
	operado = models.CharField(max_length=255, blank=True)
	cantidadTotal = models.IntegerField(blank=True)
	imagen = models.ImageField(upload_to='proyectos', blank=True)
	ubicacion = models.CharField(max_length=50, blank=True)

	def __unicode__(self):
		return self.nombre
