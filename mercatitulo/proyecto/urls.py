from django.conf.urls import patterns, url

from proyecto import views

urlpatterns = patterns(
    '',
    url(r'^$', views.proyectoVista),
    url(r'^(?P<name>[-\w]+)/$', views.proyectoVista),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.tituloVista),
)