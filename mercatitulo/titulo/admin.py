from django.contrib import admin

from .models import Titulo, TipoTitulo, TipoInmueble, Estado

admin.site.register(Titulo)
admin.site.register(TipoTitulo)
admin.site.register(TipoInmueble)
admin.site.register(Estado)