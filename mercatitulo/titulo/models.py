from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from proyecto.models import Proyecto


class TipoTitulo(models.Model):
	nombre = models.CharField(max_length=255)
	descripcion = models.TextField(max_length=500, blank=True)
	def __unicode__(self):
		return self.nombre


class TipoInmueble(models.Model):
	nombre = models.CharField(max_length=255)
	descripcion = models.TextField(max_length=500, blank=True)
	def __unicode__(self):
		return self.nombre


class Estado(models.Model):
	nombre = models.CharField(max_length=255)
	descripcion = models.TextField(max_length=500, blank=True)
	def __unicode__(self):
		return self.nombre


class Titulo(models.Model):
	proyecto = models.ForeignKey(Proyecto)
	tipoTitulo = models.ForeignKey(TipoTitulo, default=1)
	tipoInmueble = models.ForeignKey(TipoInmueble)
	vendedor = models.ForeignKey(User, null=True, blank=True, default = None)
	rentabilidad = models.FloatField()
	rentabilidad_year_1 = models.FloatField(null=True, blank=True)
	rentabilidad_year_2 = models.FloatField(null=True, blank=True)
	rentabilidad_year_3 = models.FloatField(null=True, blank=True)
	rentabilidad_year_4 = models.FloatField(null=True, blank=True)
	referido = models.TextField(null=True, blank=True)
	link_actual = models.CharField(blank=True, max_length=50)
	precio_venta = models.FloatField()
	negociable = models.CharField(max_length=2, blank=True, choices=(('0', 'Si'),('1', 'No')))
	descripcion = models.TextField(max_length=500)
	estado = models.ForeignKey(Estado, default=2)

	def __unicode__(self):
		return self.proyecto.nombre

