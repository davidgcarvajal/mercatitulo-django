# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('titulo', '0008_auto_20151202_1351'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='titulo',
            name='cantidad',
        ),
    ]
