# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('titulo', '0011_auto_20151214_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='titulo',
            name='tipoTitulo',
            field=models.ForeignKey(default=1, to='titulo.TipoTitulo'),
        ),
    ]
