# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('titulo', '0009_remove_titulo_cantidad'),
    ]

    operations = [
        migrations.AlterField(
            model_name='titulo',
            name='estado',
            field=models.ForeignKey(default=2, to='titulo.Estado'),
        ),
    ]
