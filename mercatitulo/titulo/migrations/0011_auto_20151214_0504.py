# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('titulo', '0010_auto_20151214_0502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='titulo',
            name='vendedor',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
