# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from vender.models import Vendedor
from proyecto.models import Proyecto
from titulo.models import Titulo, TipoTitulo, TipoInmueble, Estado
from django.contrib.auth.models import User

class VenderTituloForm(forms.ModelForm):
	class Meta:
		model = Titulo
		fields = ('proyecto', 'tipoInmueble', 'rentabilidad', 'precio_venta', 'negociable', 'descripcion')
		labels = {
			'proyecto': ('Proyecto/Inmueble'),
            'tipoInmueble': ('Tipo Proyecto/Inmueble'),
            'rentabilidad': ('Rentabilidad Estimada o de Cierre Año Anterior (+)'),
            'precio_venta': ('Precio de venta'),
            'descripcion': ('Descripción'),
        }
        widgets = {'tipoTitulo': forms.HiddenInput()}


class VenderTituloUsuarioForm(ModelForm):
	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'email', 'password')
		labels = {
			'first_name': ('Nombre'),
            'last_name': ('Apellido'),
            'email': ('Dirección de correo electrónico'),
            'password': ('Clave/Contraseña'),
        }		

class VenderTituloRegistroForm(ModelForm):
	class Meta:
		model = Vendedor
		exclude = ['user']
		labels = {
			'cedula': ('Cédula'),
            'telefono': ('Teléfono Celular'),
            'departamento': ('Departamento'),
            'ciudad': ('Ciudad'),
            'direccion': ('Dirección Física'),
        }					
