from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from vender.forms import VenderTituloForm, VenderTituloRegistroForm, VenderTituloUsuarioForm

def venderIndex(request):
	templateData = "Index"
	return render(request, 'vender.html', {'templateData': templateData})


def venderTitulo(request):
	if request.method=='POST':
		formulario = VenderTituloForm(request.POST)
		if formulario.is_valid():
			nuevoTitulo = formulario.save() # Guardar los datos en la base de datos
			request.session['nuevo_titulo'] = nuevoTitulo.id
			return redirect('/vender/titulo/registro', pk=nuevoTitulo.id)
	else:
		formulario = VenderTituloForm()

	
	return render(request, 'titulo.html', {'templateData': formulario})


def registro(request):
	if 'nuevo_titulo' in request.session:
	    idTitulo = request.session['nuevo_titulo']
	    data = idTitulo	

	if request.method=='POST':
		formularioUsuario = VenderTituloUsuarioForm(request.POST)
		formularioVendedor = VenderTituloRegistroForm(request.POST)
		if formularioVendedor.is_valid():
			#nuevoUsuario = formularioUsuario.save() 
			#nuevoVendedor = formularioVendedor.save()
			return redirect('/vender/titulo/confirmacion')

	else:
		formularioUsuario = VenderTituloUsuarioForm()
		formularioVendedor = VenderTituloRegistroForm()
		templateData = {
			'formularioUsuario': formularioUsuario,
			'formularioVendedor': formularioVendedor,
		}

	return render(request, 'registro.html', {'templateData': templateData})


def confirmacion(request):
	templateData = 'Index'
	return render(request, 'confirmacion.html', {'templateData': templateData})