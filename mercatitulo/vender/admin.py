from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from vender.models import Vendedor


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class VendedorInline(admin.StackedInline):
    model = Vendedor
    can_delete = False
    verbose_name_plural = 'vendedor'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (VendedorInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
