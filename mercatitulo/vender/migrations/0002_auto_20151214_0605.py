# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vender', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendedor',
            name='cedula',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='vendedor',
            name='ciudad',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='vendedor',
            name='departamento',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='vendedor',
            name='telefono',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
