# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vender', '0002_auto_20151214_0605'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendedor',
            name='direccion',
            field=models.TextField(default='', max_length=500),
            preserve_default=False,
        ),
    ]
