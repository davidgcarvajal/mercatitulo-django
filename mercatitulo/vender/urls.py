from django.conf.urls import patterns, url

from vender import views

urlpatterns = patterns(
    '',
    url(r'^$', views.venderIndex),
    url(r'^titulo/$', views.venderTitulo),
    url(r'^titulos/$', views.venderTitulo),
    url(r'^vender-derecho-fiduciario/$', views.venderTitulo),    
    url(r'^titulo/registro/$', views.registro),
    url(r'^titulo/confirmacion/$', views.confirmacion),
)
