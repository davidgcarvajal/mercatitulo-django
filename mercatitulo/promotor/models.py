from __future__ import unicode_literals

from django.db import models

class Promotor(models.Model):
	nombre = models.CharField(max_length=255)
	machineName = models.CharField(max_length=255, unique=True)
	descripcion = models.TextField(max_length=2500)
	imagen = models.ImageField(upload_to='promotor', blank=True)

	def __unicode__(self):
		return self.nombre
