from django.apps import AppConfig


class FiduciariaConfig(AppConfig):
    name = 'fiduciaria'
