from __future__ import unicode_literals

from django.db import models

class Fiduciaria(models.Model):
	nombre = models.CharField(max_length=255)
	machineName = models.CharField(max_length=255, unique=True)
	descripcion = models.TextField()
	imagen = models.ImageField(upload_to='fiduciaria', blank=True)

	def __unicode__(self):
		return self.nombre
