from django.conf.urls import patterns, url

from comprar import views

urlpatterns = patterns(
    '',
    url(r'^$', views.comprarIndex),	
    url(r'^(?P<name>[-\w]+)/$', views.comprarVista),
)
