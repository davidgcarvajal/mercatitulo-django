from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from proyecto.models import Proyecto
from titulo.models import Titulo

class Compra(models.Model):
	proyecto = models.ForeignKey(Proyecto)
	titulo = models.ForeignKey(Titulo)
	comprador = models.ForeignKey(User, null=True, blank=True, default = None)
	
	def __unicode__(self):
		return self.proyecto.proyecto	
