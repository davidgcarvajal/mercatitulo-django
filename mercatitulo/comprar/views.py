from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from comprar.forms import ComprarTituloForm

def comprarIndex(request):
	templateData = "Index"
	return render(request, 'comprar.html', {'templateData': templateData})


def comprarVista(request, name=0):
	if request.method=='POST':
		formulario = ComprarTituloForm(request.POST)
		if formulario.is_valid():
			nuevaCompra = formulario.save() # Guardar los datos en la base de datos
			request.session['nueva_compra'] = nuevaCompra.id
			return redirect('/vender/titulo/registro', pk=nuevaCompra.id)
	else:
		formulario = ComprarTituloForm()

	
	return render(request, 'compratitulo.html', {'templateData': formulario})