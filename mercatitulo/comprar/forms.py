# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from proyecto.models import Proyecto
from titulo.models import Titulo, TipoTitulo, TipoInmueble, Estado
from comprar.models import Compra
from django.contrib.auth.models import User

class ComprarTituloForm(forms.ModelForm):
	class Meta:
		model = Compra
		fields = ('proyecto', 'titulo')
		labels = {
			'proyecto': ('Proyecto/Inmueble'),
            'titulo': ('Título'),
        }
