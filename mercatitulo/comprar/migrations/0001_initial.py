# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('titulo', '0012_auto_20160208_0405'),
        ('proyecto', '0004_proyecto_operado'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comprador', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('proyecto', models.ForeignKey(to='proyecto.Proyecto')),
                ('titulo', models.ForeignKey(to='titulo.Titulo')),
            ],
        ),
    ]
